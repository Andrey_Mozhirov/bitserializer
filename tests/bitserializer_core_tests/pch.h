//
// pch.h
// Header for standard system include files.
//

#pragma once
#include <array>
#include <cstdlib>
#include <deque>
#include <filesystem>
#include <forward_list>
#include <functional>
#include <limits>
#include <list>
#include <map>
#include <optional>
#include <set>
#include <type_traits>
#include <vector>
#include <ostream>
#include <istream>
#include <fstream>

#include "gtest/gtest.h"